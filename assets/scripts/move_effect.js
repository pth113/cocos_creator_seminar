// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        playerNormal: {
            default: null,
            type: cc.Node
        },
        playerInOutCubic: {
            default: null,
            type: cc.Node
        }
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },

    // LIFE-CYCLE CALLBACKS:
    playPlayerNormal : function() {
        var jumpUp = cc.moveBy(1, cc.v2(0, 200));
        var jumpDown = cc.moveBy(1, cc.v2(0, -200));
        var action =  cc.repeatForever(cc.sequence(jumpUp, jumpDown));
        this.playerNormal.runAction(action);
    },
    playPlayerInOutCubic : function() {
        var jumpUp = cc.moveBy(1, cc.v2(0, 200)).easing(cc.easeCubicActionOut());
        var jumpDown = cc.moveBy(1, cc.v2(0, -200)).easing(cc.easeCubicActionIn());
        var action =  cc.repeatForever(cc.sequence(jumpUp, jumpDown));
        this.playerInOutCubic.runAction(action);
    },
    onLoad : function () {
        this.playPlayerNormal();
        this.playPlayerInOutCubic();
    },

    start () {

    },

    update (dt) {
        console.log('Delta time: ' + dt);
    },
});
