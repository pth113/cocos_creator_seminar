// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        collider: cc.BoxCollider,
        maxXSpeed: 10,
        runDuration : 35/60,
        xAccel : 100,
        ySpeed : 100,
        jumpDuration : 55/60,
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },

    onKeyDown (event) {
        // set a flag when key pressed
        switch(event.keyCode) {
            case cc.macro.KEY.a:
                this.accLeft = true;
                break;
            case cc.macro.KEY.d:
                this.accRight = true;
                break;
            case cc.macro.KEY.space:
                this.accJump = true;
                break;
        }
    },

    onKeyUp (event) {
        this.xSpeed = 0;        
        // unset a flag when key released
        switch(event.keyCode) {
            case cc.macro.KEY.a:
                this.accLeft = false;
                break;
            case cc.macro.KEY.d:
                this.accRight = false;
                break;
            case cc.macro.KEY.space:
                this.accJump = false;
                break;
        }
    },

    setRunBack : function(dt) {
        if (this.turnOffRun === false) {
            this.turnOffRun = true;
            var anim = this.getComponent(cc.Animation);
            anim.play('run_back');
        }
        this.setXMove(dt);
    },

    setRun : function(dt) {
        if (this.turnOffRun === false) {
            this.turnOffRun = true;
            var anim = this.getComponent(cc.Animation);
            anim.play('run');
        }
        this.setXMove(dt);
    },

    setJump : function() {
        if (this.turnOffJump === false) {
            this.turnOffJump = true;
            this.turnOffRun = true;
            var anim = this.getComponent(cc.Animation);
            anim.play('jump');
            var jumpUp = cc.moveBy(this.jumpDuration / 2, cc.v2(0, this.ySpeed));
            var jumpDown = cc.moveBy(this.jumpDuration / 2, cc.v2(0, -this.ySpeed));
            var sequence = cc.sequence(jumpUp, jumpDown);
            this.node.runAction(sequence);
        }
    },

    setXMove : function(dt) {        
        if (this.accRight === true) {
            this.xSpeed += this.xAccel * dt;
        } else {
            this.xSpeed -= this.xAccel * dt;
        }
        if ( Math.abs(this.xSpeed) > this.maxXSpeed ) {
            // if speed reach limit, use max speed with current direction
            this.xSpeed = this.maxXSpeed * this.xSpeed / Math.abs(this.xSpeed);
        }
        var move = cc.moveBy(this.runDuration, cc.v2(this.xSpeed, 0));
        this.node.runAction(move);
    },

    setIdle : function () {
        var anim = this.getComponent(cc.Animation);
        anim.play('idle');
        this.turnOffJump = false;
        this.turnOffRun = false;
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        // Acceleration direction switch
        this.accLeft = false;
        this.accRight = false;
        this.turnOffRun = false;
        // The main character's current horizontal velocity
        this.xSpeed = 0;
        // Initialize the keyboard input listening
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);

        var anim = this.getComponent(cc.Animation);       
        var _root = this;       
        anim.on('finished', function(stringFinished, animStateFinished) {
            _root.turnOffRun = false;
            _root.turnOffJump = false;
            _root.setIdle();
        });

        //collider
        var manager = cc.director.getCollisionManager();
        manager.enabled = true;
        manager.enabledDebugDraw = true;
        manager.enabledDrawBoundingBox = true;

        this.collider.node.on(cc.Node.EventType.TOUCH_START, function (touch, event) {
            // return the touch point with world coordinates
            let touchLoc = touch.getLocation();
            // https://docs.cocos.com/creator/api/en/classes/Intersection.html Intersection
            if (cc.Intersection.pointInPolygon(touchLoc, this.collider.world.points)) {
                console.log("Hit!");
            }
            else {
                console.log("No hit");
            }
        }, this);
    },

    /**
     * Call when a collision is detected
     * @param  {Collider} other The other Collider Component
     * @param  {Collider} self  Self Collider Component
     */
    onCollisionEnter: function (other, self) {
        console.log('on collision enter');

        // Collider Manager will calculate the value in world coordinate system, and put them into the world property
        var world = self.world;

        // Collider Component aabb bounding box
        var aabb = world.aabb;

        // The position of the aabb collision frame before the node collision
        var preAabb = world.preAabb;

        // world transform
        var t = world.transform;

        // Circle Collider Component world properties
        var r = world.radius;
        var p = world.position;

        // Rect and Polygon Collider Component world properties
        var ps = world.points;
    },

    onCollisionStay: function (other, self) {
        console.log('on collision stay');
    },

    onCollisionExit: function (other, self) {
        console.log('on collision exit');
    },

    onDestroy () {
        // Cancel keyboard input monitoring
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    },
    start () {
		
	},

    update : function (dt) {
        if (this.accRight) {
            this.setRun(dt);
        } else if (this.accLeft) {
            this.setRunBack(dt);
        }
        
        if (this.accJump) {
            this.setJump();
        }
    },
});
