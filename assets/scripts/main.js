// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        button_animation_clip: cc.Button,
        button_move_effects: cc.Button,
        button_collider: cc.Button,
        button_physics: cc.Button,
        button_no_gravity: cc.Button,
        button_physics_no_contact: cc.Button,
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad : function () {
        this.button_animation_clip.node.on('click', this.callback_animation_clip, this);
        this.button_move_effects.node.on('click', this.callback_move_effects, this);
        this.button_collider.node.on('click', this.callback_button_collider, this);
        this.button_physics.node.on('click', this.callback_button_physics, this);
        this.button_no_gravity.node.on('click', this.callback_button_no_gravity, this);
        this.button_physics_no_contact.node.on('click', this.callback_button_physics_no_contact, this);
    },

    callback_animation_clip: function (button) {
        cc.director.loadScene('animation_clip');
    },

    callback_move_effects: function (button) {
        cc.director.loadScene('move_effects');
    },

    callback_button_collider: function (button) {
        cc.director.loadScene('collider');
    },

    callback_button_physics: function (button) {
        cc.director.loadScene('physic');
    },

    callback_button_no_gravity: function (button) {
        cc.director.loadScene('physic_no_ground');
    },

    callback_button_physics_no_contact: function (button) {
        cc.director.loadScene('physic_no_contact');
    },

    start () {

    },

    // update (dt) {},
});
